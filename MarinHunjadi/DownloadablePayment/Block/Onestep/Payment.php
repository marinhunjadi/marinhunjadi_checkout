<?php
/**
 * Remove checkout first step
 *
 * @category   MarinHunjadi
 * @package    MarinHunjadi_DownloadablePayment
 * @author      Marin Hunjadi <marin.hunjadi@ck.htnet.hr>
 */

class MarinHunjadi_DownloadablePayment_Block_Onepage_Payment extends Mage_Checkout_Block_Onepage_Payment
{
    /**
     * Retrieve availale payment methods
     *
     * @return array
     */
    public function getMethods()
    {
        $methods = $this->getData('methods');
        if (is_null($methods)) {
            $quote = $this->getQuote();
            $store = $quote ? $quote->getStoreId() : null;
            $methods = $this->helper('payment')->getStoreMethods($store, $quote);
            $total = $quote->getBaseSubtotal() + $quote->getShippingAddress()->getBaseShippingAmount();
			$isDownloadable = false;
			if($quote){
				$cartItems = $quote->getAllVisibleItems();
				foreach ($cartItems as $item) {
					$product = Mage::getModel('catalog/product')->load($item->getId);
					if($product->getIsDownloadable()){
						$isDownloadable = true; 
						break;
					}
				}
			}
            foreach ($methods as $key => $method) {
				/*if($isDownloadable && $method->getCode() != 'checkmo' && $method->getCode() != 'cashondelivery'){
					unset($methods[$key]);
					continue;
				}*/
                if ($this->_canUseMethod($method)
                    && ($total != 0
                        || $method->getCode() == 'free'
                        || ($quote->hasRecurringItems() && $method->canManageRecurringProfiles())) && (!$isDownloadable || $method->getCode() == 'checkmo' || $method->getCode() == 'cashondelivery')) {
                    $this->_assignMethod($method);
                } else {
                    unset($methods[$key]);
                }
            }
            $this->setData('methods', $methods);
        }
        return $methods;
    }    
}
